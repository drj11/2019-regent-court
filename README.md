# 2019 Regent Court refurb plans

Fun facts

Cellular offices


Floor 01 oppsite the southern stairs on the east wing


9.9m² (nominal 10)
are 4.3 by 2.3 m (internal), including an inward opening door.

9.5m² (nominal 9)
4.3 by 2.2 m (internal), including an inward opening door.

Floor 01, south of southern stairs on east wing

8.4 (nominal 9)
3.5 by 2.4 m (internal), including an inward opening door.

This immediately suggests at 3.5 x 2.2 office at 7.7m²

Corridors are 1.6m wide nominal. Occasional projections.

# Existing dimensions

My desk in the RSE office, which I think is fairly standard, is 1600mm by 800mm.

My desk in G30 is 1834mm by 800mm
(this includes a pedestal drawer unit which is 431mm wide).

A curved desk in G30 is 1800mm by 1630mm
(this includes a pedestal drawer unit on the short side).

Office door is 900mm (for example, RSE office).

Gap behind a desk for chair/person should be 1.0m,
for back-to-back should be 2.0m.
drj notes: for two people 2.0m seems quite generous;
for one person 1.0m seems a bit tight.

I think there should be smaller meeting rooms.
In a 9m² office you can easily have a meeting with 4 people.